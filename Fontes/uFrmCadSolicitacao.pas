//TAG 1 - Inclus�o
//    2 - Altera��o
//    3 - Visualiza��o
unit uFrmCadSolicitacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Data.DB, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Mask, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  System.Actions, Vcl.ActnList, DBCtrlsEh;

type
  TFrmCadSolicitacao = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtCliente: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtVersao: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DsPrioridade: TDataSource;
    DsSituacao: TDataSource;
    DsAplicativo: TDataSource;
    cbxSituacao: TDBLookupComboBox;
    cbxAplicativo: TDBLookupComboBox;
    edtContato: TDBEdit;
    Panel1: TPanel;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    cbxSolicitante: TDBLookupComboBox;
    DsUsuario: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    memoSolicitacao: TDBMemo;
    PageControl1: TPageControl;
    tbComentario: TTabSheet;
    tbVisualizar: TTabSheet;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    Label12: TLabel;
    spdForward: TSpeedButton;
    spdBack: TSpeedButton;
    spdVoltar: TSpeedButton;
    btnAddComentario: TBitBtn;
    DBLookupComboBox2: TDBLookupComboBox;
    spdGravar: TSpeedButton;
    ActionList1: TActionList;
    ac_Gravar: TAction;
    ac_AdicionaComentario: TAction;
    ac_Cancelar: TAction;
    DsEstimativa: TDataSource;
    cbxEstimativa: TDBLookupComboBox;
    Label13: TLabel;
    cbxTpServico: TDBLookupComboBox;
    Label14: TLabel;
    DsTpServico: TDataSource;
    dtSolicitacao: TDBDateTimeEditEh;
    dtLiberacao: TDBDateTimeEditEh;
    hrInicio: TDBDateTimeEditEh;
    hrTermino: TDBDateTimeEditEh;
    lbHoraInicio: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBComboBox1: TDBComboBox;
    LookupPrioridade: TDBLookupComboBox;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure spdGravarClick(Sender: TObject);
    procedure spdVoltarClick(Sender: TObject);
    procedure spdBackClick(Sender: TObject);
    procedure spdForwardClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure ac_GravarExecute(Sender: TObject);
    procedure ac_AdicionaComentarioExecute(Sender: TObject);
    procedure ac_CancelarExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadSolicitacao: TFrmCadSolicitacao;


implementation

{$R *.dfm}

uses uDM, uFrmPrincipal;

procedure TFrmCadSolicitacao.ac_AdicionaComentarioExecute(Sender: TObject);
begin
  if not DM.QryComentario.Active then
    DM.QryComentario.Open;

  DM.QryComentario.Append;
  PageControl1.ActivePage := tbVisualizar;
  tbVisualizar.TabVisible := True;
  tbComentario.TabVisible := False;
end;

procedure TFrmCadSolicitacao.ac_CancelarExecute(Sender: TObject);
begin
 if Tag = 1 then
   DM.QrySolicitacao.CancelUpdates;
 Close;
end;

procedure TFrmCadSolicitacao.ac_GravarExecute(Sender: TObject);
begin
  if Tag = 3 then
  begin
    ShowMessage('N�o � possivel Salvar quando esta Consultando!');
    Exit;
  end;
  if LookupPrioridade.KeyValue = Null then
  begin
  ShowMessage('Favor adicionar prioridade a solicita��o!');
  Exit
  end;

  if cbxSituacao.Text = EmptyStr then
  begin
  ShowMessage('Favor adicionar situa��o a solicita��o!');
  Exit;
  end;

  if cbxAplicativo.Text = EmptyStr then
  begin
  ShowMessage('Favor adicionar um aplicativo a solicita��o!');
  Exit;
  end;

  if cbxSolicitante.Text = EmptyStr then
  begin
  ShowMessage('Favor adicionar um solicitante a solicita��o!');
  Exit;
  end;

  if edtCliente.Text = EmptyStr then
  begin
  ShowMessage('Favor adicionar um cliente a solicita��o!');
  Exit;
  end;

  if edtContato.Text = EmptyStr then
  begin
   ShowMessage('Favor adicionar um contato a solicita��o!');
   Exit;
  end;

  if cbxTpServico.Text = EmptyStr then
  begin
   ShowMessage('Favor adicionar um tipo de servico!');
   Exit;
  end;

  if (TAG = 1) and (DM.QrySolicitacaoCODESTIMATIVA.IsNull) then
   DM.QrySolicitacaoCODESTIMATIVA.Value := 1;

  DM.QrySolicitacaoDATASOLICITACAO.Value := StrToDate(dtSolicitacao.Text);
  DM.QrySolicitacaoDATALIBERACAO.Value   := StrToDate(dtLiberacao.Text);
  Dm.QrySolicitacao.ApplyUpdates(0);
  Dm.QrySolicitacao.Connection.CommitRetaining;
  Close;
end;

procedure TFrmCadSolicitacao.btnCancelarClick(Sender: TObject);
begin
 if Tag = 1 then
   DM.QrySolicitacao.CancelUpdates;
 Close;
end;

procedure TFrmCadSolicitacao.DBGrid1DblClick(Sender: TObject);
begin
 PageControl1.ActivePage := tbVisualizar;
 tbVisualizar.TabVisible := True;
 tbComentario.TabVisible := False;

end;

procedure TFrmCadSolicitacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 DM.QryComentario.Close;
 FrmPrincipal.BitBtn1.Click;
end;

procedure TFrmCadSolicitacao.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
   Perform(40, 0, 0);
 end;
 if Key = #27 then
 begin
   ac_Cancelar.Execute;
 end;
end;

procedure TFrmCadSolicitacao.FormShow(Sender: TObject);
begin
 if Tag = 1 then
 begin
  DM.QrySolicitacao.Append;
  dtSolicitacao.Text := DateToStr(Now);
  dtLiberacao.Text := '01/01/2000'
 end
 else
 begin
  dtSolicitacao.Text := DateToStr(DM.QrySolicitacaoDATASOLICITACAO.Value);
  dtLiberacao.Text   := DateToStr(DM.QrySolicitacaoDATALIBERACAO.Value);
 end;
 DM.QryComentario.Open;
 PageControl1.ActivePage := tbComentario;
 tbVisualizar.TabVisible    := False;
end;

procedure TFrmCadSolicitacao.spdBackClick(Sender: TObject);
begin
 DM.QryComentario.Prior;
end;

procedure TFrmCadSolicitacao.spdForwardClick(Sender: TObject);
begin
 DM.QryComentario.Next;
end;

procedure TFrmCadSolicitacao.spdGravarClick(Sender: TObject);
begin
 if DM.QryComentario.State in [dsEdit,dsInsert] then
 begin
  DM.QryComentarioSD_SOLICITACAO_COD.Value := DM.QrySolicitacaoCOD.Value;
  DM.QryComentarioDATA.AsDateTime  := Now;
  DM.QryComentario.Post;
  DM.QryComentario.ApplyUpdates(0);
  PageControl1.ActivePage := tbComentario;
  tbVisualizar.TabVisible := False;
  tbComentario.TabVisible := True;
 end
 else
 begin
   Exit;
 end;
end;

procedure TFrmCadSolicitacao.spdVoltarClick(Sender: TObject);
begin
 if DM.QryComentario.State in [dsEdit,dsInsert] then
   DM.QryComentario.CancelUpdates;

 PageControl1.ActivePage := tbComentario;
 tbVisualizar.TabVisible := False;
 tbComentario.TabVisible := True;
end;

end.
