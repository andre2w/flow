inherited FrmCadPessoa: TFrmCadPessoa
  Caption = 'Cadastro de Pessoas'
  ExplicitWidth = 489
  ExplicitHeight = 471
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = tbPesquisa
    inherited tbPesquisa: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 465
      ExplicitHeight = 404
    end
    inherited tbCadastro: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 465
      ExplicitHeight = 404
      object Label1: TLabel [0]
        Left = 3
        Top = 3
        Width = 39
        Height = 13
        Caption = 'PESSOA'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 3
        Top = 22
        Width = 459
        Height = 21
        CharCase = ecUpperCase
        DataField = 'PESSOA'
        DataSource = DataSource1
        TabOrder = 1
      end
    end
  end
  inherited DataSource1: TDataSource
    DataSet = DM.QryUsuario
  end
  inherited ActionList1: TActionList
    inherited ac_pesquisar: TAction
      OnExecute = ac_pesquisarExecute
    end
  end
end
