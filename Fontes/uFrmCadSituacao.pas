unit uFrmCadSituacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, System.Actions,
  Vcl.ActnList, Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Mask, Vcl.DBCtrls;

type
  TFrmCadSituacao = class(TFrmModelo)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure ac_pesquisarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadSituacao: TFrmCadSituacao;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmCadSituacao.ac_pesquisarExecute(Sender: TObject);
begin
  inherited;
  Filtrar('SITUACAO');
end;

end.
