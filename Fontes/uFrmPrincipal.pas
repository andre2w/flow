unit uFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Data.DB,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Imaging.pngimage, System.Actions,
  Vcl.ActnList, Vcl.DBCtrls, Vcl.ComCtrls, Vcl.Menus, FireDAC.Comp.Client,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask,System.UITypes , DBCtrlsEh;

type
  TFrmPrincipal = class(TForm)
    dsSolicitacao: TDataSource;
    pnTop: TPanel;
    pnBottom: TPanel;
    btnIncluir: TBitBtn;
    btnEditar: TBitBtn;
    btnVisualizar: TBitBtn;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    ac_Pesquisar: TAction;
    ac_Incluir: TAction;
    ac_Visualizar: TAction;
    ac_Editar: TAction;
    Image1: TImage;
    cbxPesquisa: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    edtPesquisa: TEdit;
    Label3: TLabel;
    lbsituacao: TLabel;
    Label5: TLabel;
    cbxPrioridade: TComboBox;
    cbxSituacao: TComboBox;
    cbxResponsavel: TComboBox;
    Label4: TLabel;
    cbxData: TComboBox;
    Label6: TLabel;
    MainMenu1: TMainMenu;
    Cadastros: TMenuItem;
    Changelog1: TMenuItem;
    GerarTXT1: TMenuItem;
    Usuario1: TMenuItem;
    Aplicacao1: TMenuItem;
    Estimativa1: TMenuItem;
    Prioridade1: TMenuItem;
    Situacao1: TMenuItem;
    EnviarparaMySQL1: TMenuItem;
    ipodeServico1: TMenuItem;
    PopupMenu1: TPopupMenu;
    EmTeste1: TMenuItem;
    dbGridPrincipal: TDBGridEh;
    dtInicial: TDBDateTimeEditEh;
    dtFinal: TDBDateTimeEditEh;
    cbxOrdenar: TComboBox;
    Label7: TLabel;
    Scripts1: TMenuItem;
    cbxAplicacao: TComboBox;
    Label8: TLabel;
    pnLeitura: TPanel;
    DBMemo1: TDBMemo;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    btModoLeitura: TBitBtn;
    Splitter1: TSplitter;
    procedure ac_IncluirExecute(Sender: TObject);
    procedure ac_EditarExecute(Sender: TObject);
    procedure ac_PesquisarExecute(Sender: TObject);
    procedure ac_VisualizarExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);

    procedure GerarTXT1Click(Sender: TObject);
    procedure Usuario1Click(Sender: TObject);
    procedure Aplicacao1Click(Sender: TObject);
    procedure Estimativa1Click(Sender: TObject);
    procedure Prioridade1Click(Sender: TObject);
    procedure Situacao1Click(Sender: TObject);
    procedure EnviarparaMySQL1Click(Sender: TObject);
    procedure ipodeServico1Click(Sender: TObject);
    procedure EmTeste1Click(Sender: TObject);
    procedure dbGridPrincipalDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure cbxPesquisaChange(Sender: TObject);
    procedure Scripts1Click(Sender: TObject);
    procedure btModoLeituraClick(Sender: TObject);
  private
    { Private declarations }
    function GeraChangelog:Boolean;
    procedure carregaCBX(AComboBox :TComboBox; AQry:TFDQuery);
    procedure configuraComboBoxes;
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

uses uDM, uFrmCadSolicitacao, uFrmCadPessoa,
  uFrmCadAplicacao, uFrmCadEstimativa, uFrmCadPrioridade, uFrmCadSituacao,
  uFrmChangelog, uFrmCadTpServico, uFrmAlteraSituacao, uFrmScripts;

procedure TFrmPrincipal.ac_EditarExecute(Sender: TObject);
begin
 if DM.QrySolicitacao.IsEmpty then
    Exit;

 FrmCadSolicitacao := TFrmCadSolicitacao.Create(Self);
 FrmCadSolicitacao.Tag := 2;
 DM.dsSolicitacao.Edit;
 FrmCadSolicitacao.ShowModal;
 FreeAndNil(FrmCadSolicitacao);
end;

procedure TFrmPrincipal.ac_IncluirExecute(Sender: TObject);
begin
 if not DM.QrySolicitacao.Active then
  DM.QrySolicitacao.Open;

 FrmCadSolicitacao := TFrmCadSolicitacao.Create(Self);
 FrmCadSolicitacao.Tag := 1;
 FrmCadSolicitacao.ShowModal;
 FreeAndNil(FrmCadSolicitacao);
end;

procedure TFrmPrincipal.ac_PesquisarExecute(Sender: TObject);
var
 datai, dataf : string;
begin
 with DM.QrySolicitacao do
 begin
   Close;
   SQL.Clear;
   SQL.Add('SELECT S.*,A.APLICACAO,P.PRIORIDADE,SI.SITUACAO, E.TEMPO, TS.TPSERVICO as Servico ');
   SQL.Add('FROM SD_SOLICITACAO S');
   SQL.Add('JOIN SD_PRIORIDADE P  ON (P.COD = S.CODPRIORIDADE)');
   SQL.Add('JOIN SD_SITUACAO   SI ON (SI.COD = S.CODSITUACAO)');
   sql.Add('JOIN SD_APLICACAO  A  ON (A.COD = S.APLICATIVO)');
   SQL.Add('JOIN SD_ESTIMATIVA E  ON (E.COD = S.CODESTIMATIVA)');
   SQL.Add('JOIN SD_TPSERVICO  TS ON (S.TPSERVICO = TS.COD)');
   SQL.Add('where 1=1');

   if (cbxPrioridade.ItemIndex <> 0) and (cbxPrioridade.Text <> EmptyStr) then
    SQL.Add('and S.codprioridade = '+ IntToStr(cbxPrioridade.ItemIndex));

   if (cbxSituacao.ItemIndex <> 0) and (cbxSituacao.Text <> EmptyStr) then
    SQL.Add('and S.codsituacao = '+ IntToStr(cbxSituacao.ItemIndex));

   if (cbxResponsavel.ItemIndex <> 0) and (cbxResponsavel.Text <> EmptyStr) then
    SQL.Add('and S.responsavel like '+ QuotedStr(cbxResponsavel.Text));

   if (cbxAplicacao.ItemIndex <> 0) and (cbxAplicacao.Text <> EmptyStr) then
    SQL.Add('and S.APLICATIVO = ' + IntToStr(cbxAplicacao.ItemIndex) );

    datai := FormatDateTime('YYYY-MM-DD',StrToDate(dtInicial.Text));
    dataf := FormatDateTime('YYYY-MM-DD',StrToDate(dtFinal.Text));

   if cbxData.ItemIndex = 0 then
   begin
    SQL.Add('and S.DATASOLICITACAO between ' + QuotedStr(datai) +' and '+ QuotedStr(dataf) );
   end
   else
   begin
    SQL.Add('and S.DATASOLICITACAO between '+ QuotedStr(datai) +' and '+ QuotedStr(dataf) );
   end;

   if edtPesquisa.Text <> EmptyStr then
   begin
    if cbxPesquisa.ItemIndex = 0 then
     SQL.Add('and S.cliente like '+ QuotedStr(edtPesquisa.Text))
    else if cbxPesquisa.ItemIndex = 1 then
     SQL.Add('and S.contato like '+ QuotedStr(edtPesquisa.Text))
    else if cbxPesquisa.ItemIndex = 2 then
     SQL.Add('and S.solicitante like '+ QuotedStr(edtPesquisa.Text))
    else if cbxPesquisa.ItemIndex = 3 then
     SQL.Add('and S.versao like '+ QuotedStr(edtPesquisa.Text))
    else
     SQL.Add('and S.solicitacao like ' + QuotedStr(edtPesquisa.Text+'%') );
   end;

   if cbxOrdenar.Text = 'PRIORIDADE' then
   begin
     SQL.Add('order by S.CODPRIORIDADE, S.DATASOLICITACAO, S.COD');
   end
   else if cbxOrdenar.Text = 'SITUA��O' then
   begin
     SQl.Add('order by S.CODSITUACAO, S.DATASOLICITACAO, S.COD');
   end
   else if cbxOrdenar.Text = 'DATA SOLICITA��O' then
   begin
     SQL.Add('order by S.DATASOLICITACAO,S.CODPRIORIDADE, S.COD');
   end;

   Open;
 end;
end;

procedure TFrmPrincipal.ac_VisualizarExecute(Sender: TObject);
begin
 if DM.QrySolicitacao.IsEmpty then
  Exit;

 FrmCadSolicitacao := TFrmCadSolicitacao.Create(Self);
 FrmCadSolicitacao.Tag := 3;
 FrmCadSolicitacao.ShowModal;
 FreeAndNil(FrmCadSolicitacao);
end;

procedure TFrmPrincipal.Aplicacao1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadAplicacao,FrmCadAplicacao);
 FrmCadAplicacao.ShowModal;
 FreeAndNil(FrmCadAplicacao);
end;

procedure TFrmPrincipal.BitBtn2Click(Sender: TObject);
begin
 if GeraChangelog then
  ShowMessage('Changelog gerado com sucesso!');
end;

procedure TFrmPrincipal.btModoLeituraClick(Sender: TObject);
begin
  pnLeitura.Visible := not pnLeitura.Visible;
end;

procedure TFrmPrincipal.configuraComboBoxes;
begin
 carregaCBX(cbxSituacao,   DM.QrySituacao);
 carregaCBX(cbxPrioridade, DM.QryPrioridade);
 carregaCBX(cbxResponsavel,DM.QryUsuario);
 carregaCBX(cbxAplicacao,  DM.QryAplicativo);
end;

procedure TFrmPrincipal.dbGridPrincipalDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
if DM.QrySolicitacaoPRIORIDADE.Value =  'URGENTE' then
 begin
  dbGridPrincipal.Canvas.Brush.Color := $3a3ab0;
  dbGridPrincipal.Canvas.Font.Color  := clWhite;
  dbGridPrincipal.DefaultDrawDataCell(Rect,dbGridPrincipal.Columns[DataCol].Field,State);
 end
 else
 if DM.QrySolicitacaoPRIORIDADE.Value = 'ALTA'  then
 BEGIN
  dbGridPrincipal.Canvas.Brush.Color := $3C13DC;
  dbGridPrincipal.Canvas.Font.Color  := clWhite;
  dbGridPrincipal.DefaultDrawDataCell(Rect,dbGridPrincipal.Columns[DataCol].Field,State);
 END
 else
 if DM.QrySolicitacaoPRIORIDADE.Value = 'MEDIA'  then
 BEGIN
  dbGridPrincipal.Canvas.Brush.Color := $20A5DA;
  dbGridPrincipal.Canvas.Font.Color  := clWhite;
  dbGridPrincipal.DefaultDrawDataCell(Rect,dbGridPrincipal.Columns[DataCol].Field,State);
 END
 else
 if DM.QrySolicitacaoPRIORIDADE.Value = 'BAIXA'  then
 BEGIN
  dbGridPrincipal.Canvas.Brush.Color := $8B8B00;
  dbGridPrincipal.Canvas.Font.Color  := clWhite;
  dbGridPrincipal.DefaultDrawDataCell(Rect,dbGridPrincipal.Columns[DataCol].Field,State);
 end
 else
 BEGIN
  dbGridPrincipal.Canvas.Brush.Color := $71B33C;
  dbGridPrincipal.Canvas.Font.Color  := clWhite;
  dbGridPrincipal.DefaultDrawDataCell(Rect,dbGridPrincipal.Columns[DataCol].Field,State);
 END;
end;

procedure TFrmPrincipal.carregaCBX(AComboBox :TComboBox; AQry:TFDQuery);
begin
 AComboBox.Clear;
 AComboBox.Items.Add(' ');
 AQry.First;
 while not AQry.Eof do
 begin
  AComboBox.Items.Add(AQry.Fields[1].AsString);
  AQry.Next;
 end;
end;



procedure TFrmPrincipal.cbxPesquisaChange(Sender: TObject);
begin
 if cbxPesquisa.Text = 'DESCRI��O' then
  edtPesquisa.CharCase := ecNormal
 else
  edtPesquisa.CharCase := ecUpperCase;
end;

procedure TFrmPrincipal.EmTeste1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmAlteraSituacao,FrmAlteraSituacao);
 FrmAlteraSituacao.cbxSituacao.ItemIndex :=  DM.QrySolicitacaoCODSITUACAO.Value;
 FrmAlteraSituacao.edVersao.Text         :=  DM.QrySolicitacaoVERSAO.AsString  ;
 FrmAlteraSituacao.dtLiberacao.Date      :=  DM.QrySolicitacaoDATALIBERACAO.Value;
 if  FrmAlteraSituacao.ShowModal = mrOk then
 begin
  try
    DM.QrySolicitacao.Edit;
    DM.QrySolicitacaoCODSITUACAO.Value := FrmAlteraSituacao.cbxSituacao.ItemIndex;
    DM.QrySolicitacaoVERSAO.AsString := FrmAlteraSituacao.edVersao.Text;
    DM.QrySolicitacaoDATALIBERACAO.Value := FrmAlteraSituacao.dtLiberacao.Date;
    DM.QrySolicitacao.ApplyUpdates(0);
  except
   on E: Exception do
   begin
     MessageDlg('Erro ao alterar situa��o'+#13+'Erro: '+e.Message,mtError,[mbOK],0);
   end;
  end;
 end;
 FreeAndNil(FrmAlteraSituacao);
end;

procedure TFrmPrincipal.EnviarparaMySQL1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmChangelog,FrmChangelog);
 FrmChangelog.ShowModal;
 FreeAndNil(FrmChangelog);
end;

procedure TFrmPrincipal.Estimativa1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadEstimativa,FrmCadEstimativa);
 FrmCadEstimativa.ShowModal;
 FreeAndNil(FrmCadEstimativa);
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
 configuraComboBoxes;
 dtInicial.Text := '01/01/2000';
 dtFinal.Text   := DateToStr(Now);
 pnLeitura.Visible := false;
end;

function TFrmPrincipal.GeraChangelog: Boolean;
var
 arq : TextFile;
begin
 if not DirectoryExists(ExtractFilePath(Application.ExeName)+'Changelog') then
  CreateDir(ExtractFilePath(Application.ExeName)+'Changelog');
 try
   AssignFile(arq,ExtractFilePath(Application.ExeName)+'changelog'+'\Changelog.txt');
   Rewrite(arq);
   Dm.QrySolicitacao.First;
   while not DM.QrySolicitacao.Eof do
   begin
    Writeln(arq,DM.QrySolicitacaoDATALIBERACAO.AsString+ ' '+ DM.QrySolicitacao.FieldByName('RESPONSAVEL').AsString);
    Writeln(arq,DM.QrySolicitacaoSOLICITACAO.AsString);
    Writeln(arq,'------------------------------------------------');
    DM.QrySolicitacao.Next;
   end;
   CloseFile(arq);
   Result := True;
 except
  raise Exception.Create('Erro ao criar arquivo.');
  Result := False;
 end;

end;

procedure TFrmPrincipal.GerarTXT1Click(Sender: TObject);
begin
 if GeraChangelog then
  ShowMessage('Changelog gerado com sucesso!');
end;

procedure TFrmPrincipal.ipodeServico1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadTpServico,FrmCadTpServico);
 FrmCadTpServico.ShowModal;
 FreeAndNil(FrmCadTpServico);
end;

procedure TFrmPrincipal.Prioridade1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadPrioridade,FrmCadPrioridade);
 FrmCadPrioridade.ShowModal;
 FreeAndNil(FrmCadPrioridade);
end;

procedure TFrmPrincipal.Scripts1Click(Sender: TObject);
begin
 FrmScripts := TFrmScripts.Create(Self);
 FrmScripts.ShowModal;
 FreeAndNil(FrmScripts);
end;

procedure TFrmPrincipal.Situacao1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadSituacao,FrmCadSituacao);
 FrmCadSituacao.ShowModal;
 FreeAndNil(FrmCadSituacao);
end;

procedure TFrmPrincipal.Usuario1Click(Sender: TObject);
begin
 Application.CreateForm(TFrmCadPessoa,FrmCadPessoa);
 FrmCadPessoa.ShowModal;
 FreeAndNil(FrmCadPessoa);
end;

end.
