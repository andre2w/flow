unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.IBBase,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, System.IniFiles;

type
  TDM = class(TDataModule)
    QrySolicitacao: TFDQuery;
    dsSolicitacao: TDataSource;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QryComentario: TFDQuery;
    dsComentario: TDataSource;
    QryUsuario: TFDQuery;
    QryPrioridade: TFDQuery;
    QrySituacao: TFDQuery;
    QryAplicativo: TFDQuery;
    MySQLLink: TFDPhysMySQLDriverLink;
    QrySolicitacaoCOD: TFDAutoIncField;
    QrySolicitacaoAPLICATIVO: TIntegerField;
    QrySolicitacaoSOLICITANTE: TStringField;
    QrySolicitacaoCLIENTE: TStringField;
    v: TStringField;
    QrySolicitacaoSOLICITACAO: TBlobField;
    QrySolicitacaoDATALIBERACAO: TDateField;
    s: TStringField;
    QrySolicitacaoVERSAO: TStringField;
    QrySolicitacaoCODPRIORIDADE: TIntegerField;
    QrySolicitacaoCODSITUACAO: TIntegerField;
    QrySolicitacaoCODUSUARIO: TIntegerField;
    QrySolicitacaoDATASOLICITACAO: TDateField;
    QrySolicitacaoAPLICACAO: TStringField;
    QrySolicitacaoPRIORIDADE: TStringField;
    QrySolicitacaoSITUACAO: TStringField;
    QryUsuarioCOD: TFDAutoIncField;
    QryUsuarioPESSOA: TStringField;
    QryPrioridadeCOD: TFDAutoIncField;
    QryPrioridadePRIORIDADE: TStringField;
    QrySituacaoCOD: TFDAutoIncField;
    QrySituacaoSITUACAO: TStringField;
    QryAplicativoCOD: TFDAutoIncField;
    QryAplicativoAPLICACAO: TStringField;
    QryComentarioCOD: TFDAutoIncField;
    QryComentarioSD_SOLICITACAO_COD: TIntegerField;
    QryComentarioCOMENTARIO: TBlobField;
    QryComentarioCODUSUARIO: TIntegerField;
    QryComentarioDATA: TDateTimeField;
    QryEstimativa: TFDQuery;
    QrySolicitacaoCODESTIMATIVA: TIntegerField;
    QrySolicitacaoTEMPO: TStringField;
    FlowAcesso: TFDConnection;
    QryEstimativaCOD: TFDAutoIncField;
    QryEstimativaTEMPO: TStringField;
    QryTpServico: TFDQuery;
    QryTpServicoCOD: TFDAutoIncField;
    QryTpServicoTPSERVICO: TStringField;
    QrySolicitacaoTPSERVICO: TIntegerField;
    QrySolicitacaoServico: TStringField;
    QrySolicitacaoHORAINICIO: TTimeField;
    QrySolicitacaoHORATERMINO: TTimeField;
    QryComentarioTIPO: TStringField;
    QryComentarioPESSOA: TStringField;
    procedure QrySolicitacaoSOLICITACAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QryComentarioCOMENTARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure FlowAcessoAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

uses
  Winapi.Windows, Vcl.Forms;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  FlowAcesso.ConnectionDefName := 'Flow';
  MySQLLink.VendorLib := ExtractFilePath(Application.ExeName)+'libmysql.dll';
  FlowAcesso.Connected := True;
end;

procedure TDM.FlowAcessoAfterConnect(Sender: TObject);
begin
  QrySituacao.Open;
  QryUsuario.Open;
  QryPrioridade.Open;
  QryAplicativo.Open;
  QryEstimativa.Open;
  QryTpServico.Open;
end;

procedure TDM.QryComentarioCOMENTARIOGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := QryComentarioCOMENTARIO.AsString;
end;

procedure TDM.QrySolicitacaoSOLICITACAOGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := QrySolicitacaoSOLICITACAO.AsString;
end;

end.
