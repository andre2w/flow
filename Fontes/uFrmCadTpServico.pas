unit uFrmCadTpServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, Vcl.Mask, Vcl.DBCtrls,
  System.Actions, Vcl.ActnList, Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TFrmCadTpServico = class(TFrmModelo)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure ac_pesquisarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadTpServico: TFrmCadTpServico;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmCadTpServico.ac_pesquisarExecute(Sender: TObject);
begin
  inherited;
  Filtrar('TPSERVICO');
end;

end.
