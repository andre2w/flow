unit uFrmScripts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Data.DB, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Datasnap.DBClient,
  Vcl.Mask, DBCtrlsEh, FireDAC.Comp.Client;

type
  TStringArray = array of string;
  TFrmScripts = class(TForm)
    MemoScripts: TDBMemoEh;
    dsScripts: TDataSource;
    cdsScripts: TClientDataSet;
    li: TDBGridEh;
    cdsScriptsCOD: TIntegerField;
    cdsScriptsRESPONSAVEL: TStringField;
    cdsScriptsVERSAO: TStringField;
    cdsScriptsSCRIPT: TBlobField;
    edVersao: TLabeledEdit;
    btnPesquisar: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cdsScriptsSCRIPTGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    function SeparaStrings(ATexto,ADelimitador:string):TStringArray;

  public
    { Public declarations }
  end;

var
  FrmScripts: TFrmScripts;
  Qry : TFDQuery;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmScripts.btnPesquisarClick(Sender: TObject);
var
 scripts :TStringArray;
  I: Integer;
begin
  Qry := TFDQuery.Create(nil);
  Qry.Connection := DM.FlowAcesso;
  Qry.CachedUpdates := True;
  cdsScripts.EmptyDataSet;
  cdsScripts.Close;
  with Qry,SQL do
  begin
    Clear;
    Close;
    Add('SELECT S.RESPONSAVEL,S.VERSAO,S.COD, C.COMENTARIO,C.TIPO, C.DATA        ');
    Add('   FROM SD_COMENTARIO C                                                 ');
    Add('   JOIN SD_SOLICITACAO S ON (C.SD_SOLICITACAO_COD = S.COD)              ');
    Add('   WHERE C.TIPO = ''SCRIPT'' AND S.VERSAO = ' + QuotedStr(edVersao.Text) );
    Open;
  end;
  Qry.First;
  cdsScripts.Open;
  cdsScripts.DisableControls;
  while not Qry.Eof do
  begin
    scripts := SeparaStrings(Qry.FieldByName('COMENTARIO').AsString,';');
    for I := 0 to Pred(Length(scripts)) do
    begin
     if Trim(scripts[i]) <> EmptyStr then
     begin
       cdsScripts.Append;
       cdsScriptsCOD.AsInteger        := Qry.FieldByName('COD').AsInteger;
       cdsScriptsRESPONSAVEL.AsString := Qry.FieldByName('RESPONSAVEL').AsString;
       cdsScriptsVERSAO.AsString      := Qry.FieldByName('VERSAO').AsString;
       cdsScriptsSCRIPT.AsString      := scripts[I];
     end;
    end;
    Qry.Next;
  end;
  cdsScripts.EnableControls;
end;

procedure TFrmScripts.cdsScriptsSCRIPTGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
 Text := cdsScriptsSCRIPT.AsString;
end;

procedure TFrmScripts.FormCreate(Sender: TObject);
begin
 cdsScripts.CreateDataSet;
end;


function TFrmScripts.SeparaStrings(ATexto, ADelimitador: string): TStringArray;
var
  txtFinal: TStringArray;
  pospv,i : Integer;
begin
 pospv := 1;
 SetLength(txtFinal,1);
 i := 0;

 while pospv > 0 do
 begin
  pospv := 0;
  pospv := Pos(ADelimitador,ATexto,1);

  if pospv > 0 then
  begin
   if Trim(ATexto) <> EmptyStr then
      txtFinal[i] := Copy(ATexto,0,pospv - 1);
  end
  else
  begin
    if Trim(ATexto) <> EmptyStr then
      txtFinal[i] := Copy(ATexto,0,length(ATexto));
  end;


  ATexto := Copy(ATexto,pospv+1,Length(ATexto));

  Inc(i);
  SetLength(txtFinal,i + 1);
 end;

 result :=  txtfinal;
end;

end.
