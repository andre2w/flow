unit uFrmModelo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.ComCtrls,
  System.Actions, Vcl.ActnList, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids;

type
  TFrmModelo = class(TForm)
    PageControl1: TPageControl;
    tbPesquisa: TTabSheet;
    tbCadastro: TTabSheet;
    pnBottom: TPanel;
    DataSource1: TDataSource;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    btnIncluirPesq: TBitBtn;
    btnVoltarPesq: TBitBtn;
    Panel1: TPanel;
    btnIncluirCad: TBitBtn;
    btnVoltarCad: TBitBtn;
    btnPesquisar: TBitBtn;
    edPesquisar: TEdit;
    Pesquisar: TLabel;
    ActionList1: TActionList;
    ac_incluir: TAction;
    ac_voltar: TAction;
    ac_pesquisar: TAction;
    btnAlterar: TBitBtn;
    ac_alterar: TAction;
    procedure ac_incluirExecute(Sender: TObject);
    procedure ac_voltarExecute(Sender: TObject);
    procedure Filtrar(ACampo:string);
    procedure FormShow(Sender: TObject);
    procedure ac_alterarExecute(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaTb(ATab:string); //Passar a tab

  public
    { Public declarations }
  end;

var
  FrmModelo: TFrmModelo;
  Incluindo: Boolean;

implementation

uses
  FireDAC.Comp.Client;

{$R *.dfm}

procedure TFrmModelo.ac_voltarExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbPesquisa then
 begin
  DataSource1.DataSet.Filtered := False;
  Close;
 end
 else if PageControl1.ActivePage = tbCadastro then
 begin
  if Incluindo then
    TFDQuery(DataSource1.DataSet).CancelUpdates;
  SelecionaTb('Pesquisa');
 end;
end;

procedure TFrmModelo.ac_alterarExecute(Sender: TObject);
begin
 DataSource1.DataSet.Edit;
 Incluindo := True;
 SelecionaTb('Cadastro');
end;

procedure TFrmModelo.ac_incluirExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbPesquisa then
 begin
  SelecionaTb('Cadastro');
  DataSource1.DataSet.Append;
  Incluindo := True;
 end
 else if PageControl1.ActivePage = tbCadastro then
 begin
  TFDQuery(DataSource1.DataSet).ApplyUpdates(0);
  TFDQuery(DataSource1.DataSet).Connection.CommitRetaining;
  SelecionaTb('Pesquisa');
  Incluindo := False;
 end;

end;

procedure TFrmModelo.Filtrar(ACampo: string);
begin
  if edPesquisar.Text <> EmptyStr then
  begin
    DataSource1.DataSet.Filter := ACampo+' like '+ QuotedStr(edPesquisar.Text+'%');
    DataSource1.DataSet.Filtered := True;
  end
  else
    DataSource1.DataSet.Filtered := False;
end;

procedure TFrmModelo.FormShow(Sender: TObject);
begin
 SelecionaTb('Pesquisa');
 Incluindo := false;
end;

procedure TFrmModelo.SelecionaTb(ATab: string);
begin
 if ATab = 'Cadastro' then
 begin
   PageControl1.ActivePage := tbCadastro;
   tbCadastro.TabVisible := True;
   tbPesquisa.TabVisible := False;
 end
 else if ATab = 'Pesquisa' then
 begin
   PageControl1.ActivePage := tbPesquisa;
   tbCadastro.TabVisible := False;
   tbPesquisa.TabVisible := True;
 end;
end;

end.
