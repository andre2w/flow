unit uFrmChangelog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  System.Actions, Vcl.ActnList, Vcl.Buttons, System.UITypes;

type
  TFrmChangelog = class(TForm)
    memChangelog: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    edVersao: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ActionList1: TActionList;
    ac_pesquisar: TAction;
    ac_enviar: TAction;
    ac_fechar: TAction;
    procedure ac_fecharExecute(Sender: TObject);
    procedure ac_pesquisarExecute(Sender: TObject);
    procedure ac_enviarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmChangelog: TFrmChangelog;

implementation

uses
  FireDAC.Comp.Client, uDM;

{$R *.dfm}

procedure TFrmChangelog.ac_enviarExecute(Sender: TObject);
var
 DBCon : TFDConnection;
 Qry : TFDQuery;
begin
 try
  try
   DBCon := TFDConnection.Create(nil);
   DBCon.ConnectionDefName := 'Changelog';
   DBCon.Connected := True;

   Qry := TFDQuery.Create(nil);
   Qry.Connection := DBCon;
   Qry.CachedUpdates := True;

   with Qry do
   begin
     SQL.Clear;
     Close;
     SQL.Add('SELECT * FROM SD_CHANGELOG');
     Open;

     Append;
     Qry.FieldByName('DATALIBERACAO').Value :=  Now;
     Qry.FieldByName('VERSAO').Value := edVersao.Text;
     Qry.FieldByName('ALTERACAO').AsString := memChangelog.Text;
     ApplyUpdates(0);
   end;
   ShowMessage('Changelog gravado com sucesso!');

  except
   on E: Exception do
   begin
     MessageDlg('Falha ao gravar changelog!'+ #13+ e.Message,mtError,[mbOK],0);
   end;
  end;
 finally
  FreeAndNil(DBCon);
  FreeAndNil(Qry);
  memChangelog.Lines.Clear;
  edVersao.Text := EmptyStr;
 end;
end;

procedure TFrmChangelog.ac_fecharExecute(Sender: TObject);
begin
  Close;
end;

procedure TFrmChangelog.ac_pesquisarExecute(Sender: TObject);
var
 Qry: TFDQuery;
begin
 try
   Qry := TFDQuery.Create(nil);
   Qry.Connection := DM.FlowAcesso;

   with Qry,SQL do
   begin
    Add(' SELECT S.*,A.APLICACAO,P.PRIORIDADE,SI.SITUACAO, E.TEMPO  ');
    Add(' FROM SD_SOLICITACAO S                                     ');
    Add(' JOIN SD_APLICACAO  A  ON (A.COD = S.APLICATIVO)           ');
    Add(' JOIN SD_PRIORIDADE P  ON (P.COD = S.CODPRIORIDADE)        ');
    Add(' JOIN SD_SITUACAO   SI ON (SI.COD = S.CODSITUACAO)         ');
    Add(' JOIN SD_ESTIMATIVA E  ON (E.COD = S.CODESTIMATIVA)        ');
    Add(' WHERE 1=1                                                 ');
    Add(' and S.versao like ' + QuotedStr(edVersao.Text)             );
    Open;
   end;
   Qry.First;
   while not Qry.Eof do
   begin
    memChangelog.Lines.Add( Qry.FieldByName('DATALIBERACAO').AsString+ ' '+ Qry.FieldByName('RESPONSAVEL').AsString);
    memChangelog.Lines.Add(Qry.FieldByName('SOLICITACAO').AsString);
    memChangelog.Lines.Add('------------------------------------------------');
    Qry.Next;
   end;
 finally
   FreeAndNil(Qry);
 end;

end;

end.
