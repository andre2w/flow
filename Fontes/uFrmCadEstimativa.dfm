inherited FrmCadEstimativa: TFrmCadEstimativa
  Caption = 'Cadastro de Estimativa'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = tbCadastro
    inherited tbCadastro: TTabSheet
      object Label1: TLabel [0]
        Left = 3
        Top = 0
        Width = 34
        Height = 13
        Caption = 'TEMPO'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 3
        Top = 16
        Width = 459
        Height = 21
        CharCase = ecUpperCase
        DataField = 'TEMPO'
        DataSource = DataSource1
        TabOrder = 1
      end
    end
  end
  inherited DataSource1: TDataSource
    DataSet = DM.QryEstimativa
  end
  inherited ActionList1: TActionList
    inherited ac_pesquisar: TAction
      OnExecute = ac_pesquisarExecute
    end
  end
end
