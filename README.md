![FLOW-logo.jpg](https://bitbucket.org/repo/4yjyzy/images/164830310-FLOW-logo.jpg)

Aplicativo criado para controlar solicitações de desenvolvimento.

Requisitos:

* Delphi XE7+ 
* EhLib
Na mesma pasta que esta o arquivo EXE também é preciso colocar os seguintes arquivos:

* libmysql.dll
* libmsqlld.dll
* FDConnectionDefs

### Conexão com o BD ###

O banco de dados deve ser configurado no arquivo FDConnectionDefs.
O BD principal é o flow, aonde sera armazenado todas as solicitações,
já o changelog serve para subir as alterações realizadas.